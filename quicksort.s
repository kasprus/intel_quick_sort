section .text

global quicksort

quicksort:
	push rbp
	mov rbp, rsp

	;push rdi ;saving first argument
	;push rsi ;saving second argument
	push r12
	;push r13
	push r14 ; tmp register
	push r15 ; register for swap
	mov rax, rsi
	sub rax, rdi
	cmp rax, 8
	jle end_of_quicksort

	mov rcx, rdi ; begining of pivot
	mov rdx, rdi
	add rdx, 8 ; begining of end of pivot

	mov r12, rdx ; iterator

beginning_of_loop:
	cmp r12, rsi
	jge end_of_loop
	;inc QWORD[rcx]
	mov r14, QWORD[r12]
	cmp r14, QWORD[rcx]
	jg end_of_conditions
	jl less_condition
equality_condition:
	mov r15, QWORD[rdx]
	mov r14, QWORD[r12]
	mov QWORD[rdx], r14
	mov QWORD[r12], r15
	add rdx, 8
	jmp end_of_conditions
less_condition:
	mov r15, QWORD[rcx]
	mov r14, QWORD[r12]
	mov QWORD[rcx], r14
	mov QWORD[r12], r15

	mov r15, QWORD[r12]
	mov r14, QWORD[rdx]
	mov QWORD[r12], r14
	mov QWORD[rdx], r15
	add rcx, 8
	add rdx, 8

	jmp end_of_conditions
;greater_condition:
	;jmp end_of_conditions

end_of_conditions:

	add r12, 8
	jmp beginning_of_loop
end_of_loop:

	;push rcx
	;push rdx

	push rdx ;1
	push rsi ;2

	push rdi ;1
	push rcx ;2

	pop rsi
	pop rdi
	call quicksort
	pop rsi
	pop rdi
	call quicksort

	;mov rdi, [rbp - 8]
	;mov rsi, [rsp + 8]
	;jmp quicksort
	;pop rdx
	;pop rcx
	;mov rdx, [rsp]
	;mov rdi, rdx
	;mov rsi, [rbp - 16]
	;jmp quicksort


end_of_quicksort:
	pop r15
	pop r14
	;pop r13
	pop r12
	;pop rsi
	;pop rdi
	mov rsp, rbp
	pop rbp
	ret
