#include <stdio.h>
#include <stdlib.h>
#include "quicksort.h"

long long int tab[26] = {5, 1, 5, 6, 2, 4, 3, 8, 9, -8, 5, 6, 2, 4, 3, 8, 1, 5, 6, 2, 4, 3, 8, -6, -1, -5};

int main() {
	quicksort(tab, tab + 26);
	for(int i = 0; i < 26; ++i) {
		printf("%lld ", tab[i]);
	}
	return 0;
}
