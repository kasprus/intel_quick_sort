CC = gcc
CFLAGS = -Wall -m64 -g

all: main.o quicksort.o
	$(CC) $(CFLAGS) -o example main.o quicksort.o

quicksort.o: quicksort.s
	nasm -f elf64 -o quicksort.o quicksort.s

main.o: main.c
	$(CC) $(CFLAGS) -c -o main.o main.c

clean:
	rm -f *.o
